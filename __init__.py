#
# Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

from waflib import Errors

# All unlocked function together with the header they are defined in and
# the arguments to use when checking for them.
all_unlocked_functions = {
    None: [
        ('getc',     ['stdio.h'], ['stdin']),
        ('getchar',  ['stdio.h'], []),
        ('putc',     ['stdio.h'], ['\'c\'', 'stdout']),
        ('putchar',  ['stdio.h'], ['\'c\''])
    ],
    '_DEFAULT_SOURCE': [
        ('clearerr', ['stdio.h'], ['stdout']),
        ('feof',     ['stdio.h'], ['stdout']),
        ('ferror',   ['stdio.h'], ['stdout']),
        ('fileno',   ['stdio.h'], ['stdout']),
        ('fflush',   ['stdio.h'], ['stdout']),
        ('fgetc',    ['stdio.h'], ['stdin']),
        ('fputc',    ['stdio.h'], ['\'c\'', 'stdout']),
        ('fread',    ['stdio.h'], ['NULL', '1', '0', 'stdin']),
        ('fwrite',   ['stdio.h'], ['NULL', '1', '0', 'stdout'])
    ],
    '_GNU_SOURCE': [
        ('fgets',    ['stdio.h'],            ['NULL', '0', 'stdin']),
        ('fputs',    ['stdio.h'],            ['"string"', 'stdout']),
        ('getwc',    ['stdio.h', 'wchar.h'], ['stdin']),
        ('getwchar', ['stdio.h', 'wchar.h'], []),
        ('fgetwc',   ['stdio.h', 'wchar.h'], ['stdin']),
        ('fputwc',   ['stdio.h', 'wchar.h'], ['L\'c\'', 'stdout']),
        ('putwchar', ['stdio.h', 'wchar.h'], ['L\'c\'']),
        ('fgetws',   ['stdio.h', 'wchar.h'], ['NULL', '0', 'stdin']),
        ('fputws',   ['stdio.h', 'wchar.h'], ['L"string"', 'stdout'])
    ]
}

def configure(conf):
    # Find which unlocked stdio functions are requested.
    if 'UNLOCKED_FUNCTIONS' in conf.env:
        unlocked_functions = conf.env['UNLOCKED_FUNCTIONS']
    else:
        # No specific functions given, so assume all.
        unlocked_functions = []
        for define_name,fns in all_unlocked_functions.items():
            for fn,hdrs,args in fns:
                unlocked_functions.append(fn)

    have_unlocked_stdio = True
    for fn in ['flockfile', 'funlockfile']:
        try:
            fragment = \
                '#include <stdio.h>\n' \
                'int main(void) {{\n' \
                '    {}(stdout);\n' \
                '    return 0;\n' \
                '}}\n'.format(fn)
            conf.check(
                    msg='Checking for function {}'.format(fn),
                    fragment=fragment,
                    defines=['_POSIX_C_SOURCE=200809L'],
                    define_name=conf.have_define(fn))
        except Errors.ConfigurationError:
            have_unlocked_stdio = False

    if have_unlocked_stdio:
        conf.define('_POSIX_C_SOURCE', '200809L', quote=False)

        for define_name, fns in all_unlocked_functions.items():
            define = False
            for fn,hdrs,args in fns:
                if not fn in unlocked_functions:
                    continue

                defines = define_name and [define_name]
                ufn = fn + '_unlocked'
                includes = '\n'.join(
                        ['#include <{}>'.format(h) for h in hdrs])
                fragment = includes + '\n' + \
                    'int main(void) {{\n' \
                    '    {}({});\n' \
                    '    return 0;\n' \
                    '}}\n'.format(ufn, ', '.join(args))
                try:
                    conf.check(
                        msg='Checking for function {}'.format(ufn),
                        fragment=fragment,
                        defines=defines,
                        define_name=conf.have_define(ufn))
                    define = True
                except Errors.ConfigurationError:
                    pass
            if define and define_name:
                conf.define(define_name, 1)
